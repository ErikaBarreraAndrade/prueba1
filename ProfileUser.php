<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Rapid start">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta name="theme-color" content="#329aae" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/iconos.css">
</head>
<body class="profile-home">
<section>
    <div class="col-xs-3 col-xs-offset-9 padding-not mgtop5">
        <img src="./images/background/logo-right-at.png" alt="AT&T" class="img-responsive">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 picture-profile">
                <img src="./images/woman.png" alt="Mujer" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 padding-not">
                <form action="?" method="post" class="form-profile">
                    <input type="text" class="form-control text-center input-morado placeholder-at-white mgbttm15" placeholder="NOMBRE COMPLETO">
                    <input type="text" class="form-control text-center input-morado-light placeholder-at-white mgbttm15" placeholder="EMPRESA">
                    <input type="text" class="form-control text-center input-green-light placeholder-at-white mgbttm15" placeholder="CIUDAD">
                    <input type="text" class="form-control text-center input-blue-light placeholder-at-white mgbttm15" placeholder="HOBBIE">
                    <div class="row mgbttm30  button-edit-profile">
                        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
                            <div class="row">
                                <div class="col-xs-6 text-center">
                                    <a data-toggle="modal" data-target="#edit-profile" class="form-control send-button-profile">
                                        EDITAR
                                    </a>
                                </div>
                                <div class="col-xs-6">
                                    <input type="submit" class="form-control send-button-profile" value="GUARDAR">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="edit-profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container-AT-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body text-center mgbttm30">
                     <h4 class="text-center"><em>
                             ¿Quieres que tus datos <br>
                             aparezcan en el directorio?
                         </em></h4>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2">
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        <a href="#" class="button-footer-modal text-uppercase">Si</a>
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <a href="#" class="button-footer-modal text-uppercase">No</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
</section>
<footer>
    <div class="col-xs-3 col-xs-offset-9 button-back mgbttm15">
        <a href="profile.php">
            <img src="./images/back.png" alt="back" class="img-responsive">
        </a>
    </div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/npm.js"></script>
<script src="js/animation-profile.js"></script>
<script>
    $(document).ready (function(){
        function footerAlign() {
            $('footer').css('display', 'block');
            $('footer').css('height', 'auto');
            var footerHeight = $('footer').outerHeight();
            $('body').css('padding-bottom', footerHeight);
            $('footer').css('height', footerHeight);
        }
        $(document).ready(function(){
            footerAlign();
        });

        $( window ).resize(function() {
            footerAlign();
        });
    });
</script>
</body>
</html>