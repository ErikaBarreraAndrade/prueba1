<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Rapid start">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta name="theme-color" content="#329aae" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/iconos.css">
</head>
<body class="index-home">
<section>
    <img src="./images/logo_index.png" alt="AT&T" class="img-responsive logo-AT">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <form action="?" method="post" class="form-index">
                    <div class="input-group mgbttm15 mgtop15">
                        <input type="email" class="form-control" placeholder="E-MAIL" required>
                        <span class="input-group-addon"><span class="glyphicon icon-at-arroba"></span></span>
                    </div>
                    <div class="input-group mgbttm15">
                        <input type="password" class="form-control" placeholder="CONTRASEÑA" required>
                        <span class="input-group-addon"><span class="glyphicon icon-at-llave"></span></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 col-xs-offset-8 col-sm-4 col-sm-offset-8 col-md-4 col-md-offset-8">
                            <input type="submit" class="form-control button-send-form" value="INICIAR">
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-8 col-sm-8 col-md-8 pass-reset">
                        <a href="#"  data-toggle="modal" data-target="#reset-pass">* Restablecer mi contraseña</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section modal -->
    <div class="modal fade" id="reset-pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container-AT-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">
                            Restablece tu Contraseña
                        </h4>
                    </div>
                    <div class="modal-body text-center">
                        Enviaremos un código a tu correo para <br>
                        establecer una nueva contraseña
                        <form action="?" method="post" class="form-modal ">
                            <div class="row">
                                <div class=" col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-xs-12">
                                    <div class="input-group mgbttm15 mgtop15">
                                        <input type="email" class="form-control" required>
                                        <span class="input-group-addon"><span class="glyphicon icon-at-arroba"></span></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
                                            <input type="submit" class="form-control send-modal" value="Enviar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Restablecer contraseña -->
    <div class="modal fade" id="reset-pass-second" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container-AT-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">
                            Restablece tu Contraseña
                        </h4>
                    </div>
                    <div class="modal-body text-center">
                        Revisa tu correo, escribe el código de <br>
                        recuperación y escribe tu nueva contraseña
                        <form action="?" method="post" class="form-modal col-md-8 col-md-offset-2">
                            <h4 class="text-center">Escribe el código de 6 dígitos</h4>
                            <div class="input-group mgbttm15 mgtop15">
                                <input type="text" class="form-control" required>
                                <span class="input-group-addon"><span class="glyphicon icon-at-candado"></span></span>
                            </div>
                            <h4 class="text-center">Escribe tu nueva contraseña</h4>
                            <div class="input-group mgbttm15 mgtop15">
                                <input type="text" class="form-control" required>
                                <span class="input-group-addon"><span class="glyphicon icon-at-candado"></span></span>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
                                    <input type="submit" class="form-control send-modal" value="Restablecer">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Restablecer contraseña -->
    <!-- Section modal -->
</section>
<footer>
    <div class="col-xs-12 col-sm-12 col-md-12 mgtop60">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-5">
                <img src="./images/background/logo-index-at.png" alt="At&T" class="img-responsive">
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/npm.js"></script>
<script>
    $(document).ready (function(){
        function footerAlign() {
            $('footer').css('display', 'block');
            $('footer').css('height', 'auto');
            var footerHeight = $('footer').outerHeight();
            $('body').css('padding-bottom', footerHeight);
            $('footer').css('height', footerHeight);
        }
        $(document).ready(function(){
            footerAlign();
        });

        $( window ).resize(function() {
            footerAlign();
        });
    });
</script>
</body>
</html>