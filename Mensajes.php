<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Rapid start">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta name="theme-color" content="#329aae" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/iconos.css">
</head>
<body class="bg-profile-title-big mensajes">
<section>
    <div class="col-xs-12 padding-not">
        <div class="col-xs-3 agenda-image-section padding-not text-center">
            <img src="./images/mensaje.png" alt="Mensajes" class="img-responsive">
        </div>
        <div class="col-xs-6 mgtop30">
            <img src="./images/logo_secciones.png" alt="Secciones" class="img-responsive">
        </div>
        <div class="col-xs-3 padding-not mgtop5">
            <img src="./images/background/logo-right-at.png" alt="AT&T" class="img-responsive">
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 mgtop30">
               <div class="row">
                   <div class="col-xs-3 people-message">
                       <img src="./images/man.png" alt="Hombre" class="img-responsive img-circle">
                   </div>
                   <div class="col-xs-9 container-text-message">
                       <div class="container-text-message">
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, commodi
                           vitae. Consequatur cupiditate
                       </div>
                   </div>
               </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-9 col-xs-offset-3 mgtop30">
                        <div class="balloon">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, commodi
                            vitae. Consequatur cupiditate
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="col-xs-3 col-xs-offset-9 button-back mgbttm15">
        <a href="profile.php">
            <img src="./images/back.png" alt="back" class="img-responsive">
        </a>
    </div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/npm.js"></script>
<script src="js/animation-profile.js"></script>
<script>
    $(document).ready (function(){
        function footerAlign() {
            $('footer').css('display', 'block');
            $('footer').css('height', 'auto');
            var footerHeight = $('footer').outerHeight();
            $('body').css('padding-bottom', footerHeight);
            $('footer').css('height', footerHeight);
        }
        $(document).ready(function(){
            footerAlign();
        });

        $( window ).resize(function() {
            footerAlign();
        });
    });
</script>
</body>
</html>