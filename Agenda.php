<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Rapid start">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta name="theme-color" content="#329aae" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/iconos.css">
</head>
<body class="agenda">
<section>
    <div class="col-xs-12 padding-not">
        <div class="col-xs-3 agenda-image-section padding-not">
            <img src="./images/agenda.png" alt="Agenda" class="img-responsive">
        </div>
        <div class="col-xs-6 mgtop30">
            <img src="./images/logo_secciones.png" alt="Secciones" class="img-responsive">
        </div>
        <div class="col-xs-3 padding-not mgtop5">
            <img src="./images/background/logo-right-at.png" alt="AT&T" class="img-responsive">
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 mgtop30">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active list-at-panel"><a href="#dia1" aria-controls="dia1" role="tab" data-toggle="tab">Mie 06</a></li>
                        <li role="presentation" class="list-at-panel"><a href="#dia2" aria-controls="dia2" role="tab" data-toggle="tab">Jue 07</a></li>
                        <li role="presentation" class="list-at-panel"><a href="#dia3" aria-controls="dia3" role="tab" data-toggle="tab">Vie 08</a></li>
                        <li role="presentation" class="list-at-panel"><a href="#dia4" aria-controls="dia4" role="tab" data-toggle="tab">Sáb 09</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 padding-not mgbttm65">
        <div class="tab-content mgtop25">
            <div role="tabpanel" class="tab-pane fade in active" id="dia1">
                <div class="col-xs-12 padding-not">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>7:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-1">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Yoga</h4>
                            <p>Vida en Balance</p>
                            <p>07:00 AM - 08:00 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>8:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-2">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Desayuno Libre</h4>
                            <p>N/A</p>
                            <p>08:00 AM - 10:00 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>10:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-3">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 1 VP</p>
                            <p>10:00 AM - 10:30 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>11:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-4">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 2 VP</p>
                            <p>11:00 AM - 11:30 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>12:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-5">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 3 VP</p>
                            <p>12:00 AM - 12:30 AM</p>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="dia2">
                <div class="col-xs-12 padding-not">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>7:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-1">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Yoga</h4>
                            <p>Vida en Balance</p>
                            <p>07:00 AM - 08:00 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>8:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-2">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Desayuno Libre</h4>
                            <p>N/A</p>
                            <p>08:00 AM - 10:00 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>10:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-3">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 1 VP</p>
                            <p>10:00 AM - 10:30 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>11:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-4">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 2 VP</p>
                            <p>11:00 AM - 11:30 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>12:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-5">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 3 VP</p>
                            <p>12:00 AM - 12:30 AM</p>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="dia3">
                <div class="col-xs-12 padding-not">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>7:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-1">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Yoga</h4>
                            <p>Vida en Balance</p>
                            <p>07:00 AM - 08:00 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>8:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-2">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Desayuno Libre</h4>
                            <p>N/A</p>
                            <p>08:00 AM - 10:00 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>10:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-3">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 1 VP</p>
                            <p>10:00 AM - 10:30 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>11:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-4">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 2 VP</p>
                            <p>11:00 AM - 11:30 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>12:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-5">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 3 VP</p>
                            <p>12:00 AM - 12:30 AM</p>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="dia4">
                <div class="col-xs-12 padding-not">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>7:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-1">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Yoga</h4>
                            <p>Vida en Balance</p>
                            <p>07:00 AM - 08:00 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>8:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-2">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Desayuno Libre</h4>
                            <p>N/A</p>
                            <p>08:00 AM - 10:00 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>10:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-3">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 1 VP</p>
                            <p>10:00 AM - 10:30 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>11:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-4">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 2 VP</p>
                            <p>11:00 AM - 11:30 AM</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-not mgtop5">
                    <div class="col-xs-4 padding-not time-calendar text-center">
                        <h2><strong>12:00</strong></h2>
                    </div>
                    <div class="col-xs-8 padding-not container-text-event event-5">
                        <div class="text-calendar-event">
                            <h4 class="text-uppercase title-event">Conferencia</h4>
                            <p>Speaker 3 VP</p>
                            <p>12:00 AM - 12:30 AM</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="col-xs-3 col-xs-offset-9 button-back mgbttm15">
        <a href="profile.php">
            <img src="./images/back.png" alt="back" class="img-responsive">
        </a>
    </div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/npm.js"></script>
<script src="js/animation-profile.js"></script>
<script>
    $(document).ready (function(){
        function footerAlign() {
            $('footer').css('display', 'block');
            $('footer').css('height', 'auto');
            var footerHeight = $('footer').outerHeight();
            $('body').css('padding-bottom', footerHeight);
            $('footer').css('height', footerHeight);
        }
        $(document).ready(function(){
            footerAlign();
        });

        $( window ).resize(function() {
            footerAlign();
        });
    });
</script>
</body>
</html>