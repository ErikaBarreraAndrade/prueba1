<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Rapid start">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta name="theme-color" content="#329aae" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/iconos.css">
</head>
<body class="bg-profile-title-big info-generalities tipsSeguridad">
<section>
    <div class="col-xs-12 padding-not">
        <div class="col-xs-3 agenda-image-section padding-not text-center">
            <img src="./images/tipsSeguridad.png" alt="Tips de Seguridad" class="img-responsive">
        </div>
        <div class="col-xs-6 mgtop30">
            <img src="./images/logo_secciones.png" alt="Secciones" class="img-responsive">
        </div>
        <div class="col-xs-3 padding-not mgtop5">
            <img src="./images/background/logo-right-at.png" alt="AT&T" class="img-responsive">
        </div>
    </div>
    <div class="container">
        <div class="row ">
            <div class="col-xs-12 mgtop30 mgbttm30">
                <h4 class="title-security text-center">Tips de Seguridad</h4>
            </div>
            <div class="col-xs-10 col-xs-offset-1 text-security-container">
                <p>
                    Lorem Ipsum es simplemente el texto de relleno de las imprentas
                    y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar
                    de las industrias desde el año 1500, cuando un impresor (N. del T. persona
                    que se dedica a la imprenta) desconocido usó una galería de textos y los
                    mezcló de tal manera que logró hacer un libro de textos especimen.
                </p>
                <p>
                    No sólo sobrevivió 500 años, sino que tambien ingresó como texto
                    de relleno en documentos electrónicos, quedando esencialmente igual al original.
                    Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales
                    contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición,
                    como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.
                </p>
                <p>No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos
                    electrónicos, quedando esencialmente igual al original.</p>
                <p> Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de
                </p>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="col-xs-3 col-xs-offset-9 button-back mgbttm15">
        <a href="profile.php">
            <img src="./images/back.png" alt="back" class="img-responsive">
        </a>
    </div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/npm.js"></script>
<script src="js/animation-profile.js"></script>
<script>
    $(document).ready (function(){
        function footerAlign() {
            $('footer').css('display', 'block');
            $('footer').css('height', 'auto');
            var footerHeight = $('footer').outerHeight();
            $('body').css('padding-bottom', footerHeight);
            $('footer').css('height', footerHeight);
        }
        $(document).ready(function(){
            footerAlign();
        });

        $( window ).resize(function() {
            footerAlign();
        });
    });
</script>
</body>
</html>