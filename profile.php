<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Rapid start">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta name="theme-color" content="#329aae" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body class="profile-user">
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                        <img src="./images/background/logo-profile-at.png" alt="Profile-AT&T" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <h3 class="title-AT text-center">
                    Convención de ventas <br>
                    AT&T 2017
                </h3>
            </div>
            <div class="col-xs-12 menu-option-AT">
                <div class="row">
                    <div class="col-xs-6 col-sm-3 col-sm-offset-2" id="perfil">
                        <a href="ProfileUser.php">
                            <div class="media media-profile">
                                <div class="media-left">
                                    <img src="./images/b_perfil.png" alt="Perfil AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading mgtop10">Perfil</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-sm-offset-3" id="agenda">
                        <a href="Agenda.php">
                            <div class="media">
                                <div class="media-left">
                                    <img src="./images/b_agenda.png" alt="Agenda AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading mgtop10">Agenda</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row mgtop40">
                    <div class="col-xs-6 col-sm-4 col-sm-offset-2" id="participantes">
                        <a href="Participantes.php">
                            <div class="media media-user-active">
                                <div class="media-left">
                                    <img src="./images/b_participantes.png" alt="Participantes AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading mgtop10">Participantes</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-1" id="informacion">
                        <a href="InformacionGeneral.php">
                            <div class="media media-generalities-info">
                                <div class="media-left">
                                    <img src="./images/b_info.png" alt="Informacion AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading text-center mgtop10">Información General</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row mgtop25">
                    <div class="col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-2" id="vestimenta">
                        <a href="CodigoVestimenta.php">
                            <div class="media media-user-active">
                                <div class="media-left">
                                    <img src="./images/b_codigo.png" alt="Código de Vestimenta AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading text-center mgtop10"> Código de Vestimenta</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-2" id="seguridad">
                        <a href="TipsSeguridad.php">
                            <div class="media media-generalities-info">
                                <div class="media-left">
                                    <img src="./images/b_tips.png" alt="Tips AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading text-center mgtop10">Tips de Seguridad</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row mgtop30">
                    <div class="col-xs-6 col-sm-2 col-sm-offset-3 col-md-2 col-md-offset-2" id="vuelos">
                        <a href="Vuelos.php">
                            <div class="media media-user-active">
                                <div class="media-left">
                                    <img src="./images/b_vuelos.png" alt="Vuelos AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading text-center mgtop10"> Vuelos</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-5 col-xs-offset-1 col-sm-2 col-sm-offset-3" id="fotos">
                        <a href="Fotos.php">
                            <div class="media media-generalities-info">
                                <div class="media-left">
                                    <img src="./images/b_fotos.png" alt="Fotos AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading text-center mgtop10">Fotos</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row mgtop30">
                    <div class="col-xs-6 col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-2" id="mensajes">
                        <a href="Mensajes.php">
                            <div class="media media-user-active">
                                <div class="media-left">
                                    <img src="./images/b_mensajes.png" alt="Mensajes AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading text-center mgtop10"> Mensajes</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-5 col-xs-offset-1 col-sm-2 col-sm-offset-2 col-md-2 col-md-offset-3" id="faq">
                        <a href="Faq.php">
                            <div class="media media-generalities-info">
                                <div class="media-left">
                                    <img src="./images/b_faq.png" alt="Preguntas Frecuentes AT&T" class="">
                                </div>
                                <div class="media-body body-menu-At">
                                    <h6 class="media-heading text-center mgtop10">FAQ</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="col-xs-12" id="imagen-mujer">
        <img src="./images/background/woman_profile.png" alt="AT&T" class="img-responsive">
    </div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/npm.js"></script>
<script src="js/animation-profile.js"></script>
<script>
    $(document).ready (function(){
        function footerAlign() {
            $('footer').css('display', 'block');
            $('footer').css('height', 'auto');
            var footerHeight = $('footer').outerHeight();
            $('body').css('padding-bottom', footerHeight);
            $('footer').css('height', footerHeight);
        }
        $(document).ready(function(){
            footerAlign();
        });

        $( window ).resize(function() {
            footerAlign();
        });
    });
</script>
</body>
</html>